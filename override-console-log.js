const methods = ['log','error','warn','info'];
methods.forEach((method)=>{
  var f = console[method];
  console['_' + method ] = f;
  console[method] = function(){
    fetch('http://10.0.3.2:3000/log', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({log: {
        method,
        data: arguments[0],
      }}),
    }).then(() => {}).catch(() => {});
    f.apply(console, arguments);
  };
});
