/* eslint no-console: 0 */

const path = require('path');
const express = require('express');
const http = require('http');

const port = 3000;
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true,
}));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/build'));
app.get('*', function response(req, res) {
  res.sendFile(path.join(__dirname, 'build/index.html'));
});

const server = http.createServer(app);

const io = require('socket.io').listen(server);

app.post('/log', function response(req, res) {
  io.emit('PRINT_LOG', req.body.log);
  res.end();
});

server.listen(port, '0.0.0.0', function onStart(err) {
  if (err) {
    console.log(err);
  }
  console.info('==> 🌎 Listening on port %s. Open up http://0.0.0.0:%s/ in your browser.', port, port);
});
